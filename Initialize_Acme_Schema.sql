/******************************************************************************
**  Name: Script SQL Data Base Initialize "Acme"
**
**  Authors:	Marvin Dickson Mendia Calizaya
**
**  Date: 06/19/2018
*******************************************************************************
**                            Change History
*******************************************************************************
**   Date:          Author:                         Description:
** --------     -------------     ---------------------------------------------
** 06/19/2018   Marvin Mendia		Initial version
** 06/22/2018   Miguel Lopez        Employee Initialization Data
*******************************************************************************/
USE Acme;

SET XACT_ABORT ON;
SET NOCOUNT ON;

BEGIN TRANSACTION;

PRINT 'Insert data into the Area table...';
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('Elc-01','Electricity', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('hew-01','Heavy Work', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('fwo-01','Fine Work', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('roo-01','Roofed', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('fro-01','Fake Roof', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('iog-01','Installation of Gas', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('flo-01','Flooring', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('ins-01','Inspection', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('wsp-01','Work Supervision', 100);
INSERT INTO [dbo].[Area](code,name,ModifiedBy)
VALUES ('hid-01','Hidrosanitary', 100);
PRINT 'Area table done...';

PRINT 'Insert data into the Training table...';
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('Elc001','Industrial Electricity','Ing. Rocabado Marcelo',1,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('Elc002','Home Electricity','Ing. Rocabado Marcelo',1,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('rco001','Reinforced Concrete','Ing. Flores Antonio',2,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('hid002','Hidrosanitary Normative','Arq. Romero Luis',10,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('Elc003','Industrial Electricity upgrade','Ing. Campero Jose',1,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('Elc004','Home Eletricity Normative','Ing. Daza Enrique',1,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('igs001','Installation of Gas Normative','Ing. Romero Antonio',6,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('far001','Fake Roof Upgrade','Arq. Gutierres Maria',5,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('qa001','QA','Ing. Fernandez Rolando',8,100);
INSERT INTO [dbo].[Training](code,name,instructor,area_id,ModifiedBy)
VALUES ('rco002','Reinforced Concrete Normative','Ing. Flores Antonio',2,100);
PRINT 'Training table done...';


PRINT 'Insert data into the Role table...';
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('hem-001','Home Electicity Manager',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('hes-001','Home Electicity Supervisor',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('opp-001','Operator',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('het-001','Electicity Technical',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('het-002','Home Electicity Technical',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('iem-001','Industrial Electicity Manager',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('hwm-001','Heavy Work Manager',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('hws-001','Fine Work Supervisor',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('him-001','Hidrosanitary Manager',100);
INSERT INTO [dbo].[Role](code,[description],ModifiedBy)
VALUES ('his-001','Hidrosanitary Supervisor',100);
PRINT 'Role table done...';

PRINT 'Insert data into the Position table...';
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Electricity Manager',1,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Electricity Supervisor',2,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Instal Hidrosanitary',3,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Hidrosanitary Manager',9,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Hidrosanitary Supervisor',10,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Operator Heavy Work',3,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Operator Electricity',1,100);
INSERT INTO [dbo].[Position](name,Role_id,ModifiedBy)
VALUES ('Fine Work Manager',8,100);
PRINT 'Position table done...';

PRINT 'Insert data into the Employee table...';

SET IDENTITY_INSERT [dbo].[Employee] ON 

INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) 
VALUES (1, N'4507569', N'Miguel', N'Lopez', N'av guillermo urquidi', 4231247, N'm@m.com', N'good driver', N'driver', 100, CAST(N'2018-06-20 22:27:23.723' AS DateTime), 100, CAST(N'2018-06-20 22:27:23.723' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (2, N'232323cb', N'Dave', N'Roberts', N'Av. ayacucho', 4647151, N'daveRbts@hotmail.com', N'good driver', N'Driver', 100, CAST(N'1900-01-01 00:00:00.000' AS DateTime), 100, CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (3, N'25546', N'Marcelo', N'Equise', N'av villazon s/n', 4567897, N'marcelo@hotmail.com', N'constructor', N'constructor', 100, CAST(N'2018-06-21 05:31:01.990' AS DateTime), 100, CAST(N'2018-06-21 05:31:01.990' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (4, N'12354cb', N'Jorge', N'Posada', N'san martin 456', 70700001, N'jposada@hotmail.com', N'Good driver', N'driver', 100, CAST(N'2018-06-21 05:35:18.390' AS DateTime), 100, CAST(N'2018-06-21 05:35:18.390' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (5, N'335293or', N'Rubert ', N'Alba', N'av capitan Ustariz', 70789909, N'alba@hotmail.com', N'chief', N'chief', 100, CAST(N'2018-06-21 05:36:50.283' AS DateTime), 100, CAST(N'2018-06-21 05:36:50.283' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (6, N'798772lp', N'Ivan', N'Paniagua', N'Av blanco galindo', 45078990, N'ivanp@gmail.com', N'guard', N'guard', 100, CAST(N'2018-06-21 05:38:06.297' AS DateTime), 100, CAST(N'2018-06-21 05:38:06.297' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (7, N'677769sc', N'Carolina', N'Andrade', N'calle los ceibos', 67805477, N'candrade@hotmail.com', N'secratary', N'secretary', 100, CAST(N'2018-06-21 05:39:46.970' AS DateTime), 100, CAST(N'2018-06-21 05:39:46.970' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (8, N'7654456or', N'Mario', N'Bross', N'calle 16 de julio 655', 4557768, N'bross@acme.com', N'plumber', N'Plumber', 100, CAST(N'2018-06-21 05:41:41.420' AS DateTime), 100, CAST(N'2018-06-21 05:41:41.420' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (9, N'798080lp', N'Ernesto', N'Cadima', N'calle junin 789', 4321345, N'cadimaernesto@gmail.com', N'account manager', N'account manager', 100, CAST(N'2018-06-21 05:43:48.140' AS DateTime), 100, CAST(N'2018-06-21 05:43:48.140' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (10, N'765666', N'Pedro', N'Picapiedra', N'calle espana 345', 4321345, N'pedrop@gmail.com', N'supervisor', N'supervisor', 100, CAST(N'2018-06-21 05:45:22.607' AS DateTime), 100, CAST(N'2018-06-21 05:45:22.607' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (11, N'4556577cb', N'Pablo', N'Marmol', N'calle jordan 900', 77606679, N'pablomp@gmail.com', N'builder', N'builder', 100, CAST(N'2018-06-21 05:47:00.390' AS DateTime), 100, CAST(N'2018-06-21 05:47:00.390' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (12, N'4567890cb', N'Demetrio', N'Angulo', N'calle mayor rocha 123', 65455888, N'angulo@gmail.com', N'builder', N'builder', 100, CAST(N'2018-06-21 05:48:12.080' AS DateTime), 100, CAST(N'2018-06-21 05:48:12.080' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (13, N'8979778cb', N'Claudia', N'Mendieta', N'av heroinas 234', 76512300, N'claudia@gmail.com', N'secretary', N'secretary', 100, CAST(N'2018-06-21 05:49:49.183' AS DateTime), 100, CAST(N'2018-06-21 05:49:49.183' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (14, N'8979778cb', N'Maria', N'Gumucio', N'calle Calama 567', 76512300, N'mariag@gmail.com', N'builder', N'builder', 100, CAST(N'2018-06-21 05:51:23.220' AS DateTime), 100, CAST(N'2018-06-21 05:51:23.220' AS DateTime))
INSERT [dbo].[Employee] ([Id], [Dni], [First_Name], [Last_Name], [Address], [Phone], [email], [Job_Description], [Job_Position], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate])
VALUES (15, N'878787cb', N'Juan', N'Perez', N'calle dario montano 494', 76512300, N'jperez@gmail.com', N'builder', N'builder', 100, CAST(N'2018-06-21 05:52:20.810' AS DateTime), 100, CAST(N'2018-06-21 05:52:20.810' AS DateTime))
	
SET IDENTITY_INSERT [dbo].[Employee] OFF
	
	PRINT 'Employee table done...';

PRINT 'Insert data into the Employee_Training table...';
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (1,1,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (1,2,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (2,1,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (3,1,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (4,2,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (5,5,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (1,3,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (3,1,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (2,2,'ACTIVO',100);
INSERT INTO [dbo].[Employee_Training](employee_id,training_id,[state],ModifiedBy)
VALUES (3,5,'ACTIVO',100);
PRINT 'Employee_Training table done...';

COMMIT TRANSACTION;